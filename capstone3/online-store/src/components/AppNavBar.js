import { Container, Navbar, Nav } from "react-bootstrap";
import { Link,NavLink } from "react-router-dom";

import { useContext } from "react";
import UserContext from "../UserContext";


export default function AppNavBar(){
  const {user} = useContext(UserContext)
    return(
      <Navbar bg="dark" variant="dark" fixed="top">
        <Container>
          <Navbar.Brand as = {Link} to = '/'><h3>Tinker</h3></Navbar.Brand>
            <Nav className="ms-auto">
              {
                user.id ?
                
                    (user.isAdmin ?
                       <>
                        <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
                        <Nav.Link as = {NavLink} to = '/dashboard'>Dash Board</Nav.Link>
                        <Nav.Link as = {NavLink} to = '/logout' >Logout</Nav.Link>
                       </>
                       :
                        <>
                            <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/logout' >Logout</Nav.Link>
                        </>
                       

                    )
                
                :
                  <>
                    <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                    <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
                    <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
                  </>
                  
              }
              
              
          </Nav>
        </Container>
      </Navbar>

    )
}