import { Container, Row, Col, Card, Button, ButtonGroup } from "react-bootstrap";
import { Link, Navigate, useNavigate,  } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext"
import Swal2 from "sweetalert2";
import { useParams } from "react-router-dom";

export default function AdminProduct(props){
  
    
    const { user } = useContext(UserContext);
    const { productId } = useParams();  
    const { _id, name, description, price, stock, isActive } = props.productProp;
    const navigate = useNavigate();

    function archiveProduct(event){
      
      event.preventDefault();
      fetch(`${process.env.REACT_APP_API_URL}/product/${_id}/archiveProduct`,{
        method : 'PATCH',
        headers : {
          Authorization : `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then((data) =>{
        if(data ===true){
          Swal2.fire({
            "title":"Product Archived",
            icon: "success",
            text: "Product successfully archived. Proceed to dashboard"
        }).then(()=>{
          navigate("/dashboard")
        });
          
        }else{
          Swal2.fire({
            title: 'There has been an error.',
            icon: 'error',
            text : 'There was an error in your request. Please try again'
          })
        }
      })
    }

    function UpdateProduct(){
      navigate(`/updateProduct:/${_id}`)
    }

    

    return(
        <tr>
        <td>{name}</td>
        <td>{description}</td>
        <td>{price}</td>
        <td>{stock}</td>
        <td>{isActive ? "yes" : "no"}</td>
        <td>
          <ButtonGroup vertical>
            <Button className="rounded-0" variant="success" onClick={UpdateProduct}>Update</Button>
            {isActive ?
              <Button className="rounded-0" variant="danger" onClick={archiveProduct}>Archive</Button>
              :
              <Button className="rounded-0" variant="secondary" disabled>Archive</Button>
              }
          </ButtonGroup>
        </td>
      </tr>
    )
}