import { Container, Row, Col, Card, Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext"

export default function FeaturedProduct(props){
    
    const { user } = useContext(UserContext);
    const { _id, name, description, price, stock, isActive } = props.productProp;

    

    return(
                <Col className="col-12 col-lg-4 mt-3 mx-auto">
                    <Card className="rounded-0" style={{ height: '16rem' }}>
                        <Card.Header>Featured Products</Card.Header>
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <h6>Description</h6>
                            <Card.Text>
                            {description}
                            </Card.Text>
                        
                        </Card.Body>
                        <Card.Footer>
                                 <Card.Text as = {Link} to = {`/productId:/${_id}` }>
                                   { isActive ? "Available" : "Available"}
                                </Card.Text>
                               </Card.Footer>
                    </Card>
                </Col>
    )
}