import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext"

export default function ProductsCard(props){
    
    const { user } = useContext(UserContext);
    const { _id, name, description, price, stock } = props.productProp;

    

    return(
                <Col className=" col-12 col-lg-6 mt-3 mx-auto ">
                    <Card className="rounded-0">
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <h6>Description</h6>
                            <Card.Text>
                            {description}
                            </Card.Text>

                            <h6>Price</h6>
                            <Card.Text>PhP 
                            {price}
                            </Card.Text>

                            
                        </Card.Body>
                        <Card.Footer className="text-center ">
                        <small className="d-grid gap-2">
                       {
                        user.isAdmin === true ?
                        <Button as = {Link} to = {`/productId:/${_id}`} className="rounded-0"> AdminProduct Info
                        </Button>
                        :
                        <Button as = {Link} to = {`/productId:/${_id}`} className="rounded-0">Product Info
                        </Button>
                       }
                        </small>
                        </Card.Footer>
                    </Card>
                </Col>
    )
}