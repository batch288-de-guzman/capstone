import { useState, useEffect, useContext  } from "react";
import { Card, Container, Row, Col, Button } from "react-bootstrap";
import { useParams, useNavigate} from "react-router-dom";
import Swal2 from "sweetalert2";
import { Link } from "react-router-dom";

import UserContext from "../UserContext"


    
    


export default function ProductView(){
    const[name, setName] = useState(``);
    const [description, setDescription] = useState(``)
    const [price, setPrice] = useState(``)
    const [stock, setStock] = useState(``)

    const{ productId } = useParams();
    const navigate = useNavigate();
    const { user } = useContext(UserContext);

    


    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/product/productId:/${productId}`)
        .then(result => result.json())
        .then(data =>{

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setStock(data.stock);
        })
    },[])

    const buy = (productId)=>{
        fetch(`${process.env.REACT_APP_API_URL}/user/createOrder`,{
            method : 'POST',
            headers : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                id: productId
            })
        })
        .then(response => response.json())
        .then(data =>{
            if(data){
                Swal2.fire({
                    title: 'Order Successful',
                    icon: 'success',
                    text: 'Order created! Thank you for your patronage'
                })
                navigate('/products')
            }else{
                Swal2.fire({
                    title : 'Something went wrong',
                    icon: 'error',
                    text : 'There was an error with your purchase. Please try again'
                })
            }
        })
    }
    return(
        <Container className="mt-5 pt-3">
            <Row>
                <Col className="col-lg-8 mx-auto">
                <h2 className="text-center text-light pt-2">Product information</h2>
                    <Card className="rounded-0">
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>

                            <h6>Description</h6>
                            <Card.Text>
                            {description}
                            </Card.Text>

                            <h6>Price</h6>
                            <Card.Text>PhP 
                            {price}
                            </Card.Text>

                            
                            
                           
                            
                        </Card.Body>
                        <Card.Footer>
                            <small className="d-grid gap-2">
                            {
                                user.id !== null ?
                                <Button onClick = {()=> buy(productId)} className="rounded-0">Buy</Button>
                                :
                                <Button as = {Link} to = '/login' className="rounded-0">Log in to buy</Button>
                            }
                            </small>
                        </Card.Footer>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}