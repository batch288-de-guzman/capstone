import { useState, useContext, useEffect } from "react"
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap"
import { Navigate, useNavigate, useParams } from "react-router-dom";
import Swal2 from "sweetalert2";
import UserContext from "../UserContext";





export default function UpdateProduct(){
    const {user} = UserContext

    const [name, setName] = useState('');
    const [description, setDescription] = useState ('');
    const [price, setPrice] = useState ('');
    const [stock, setStock] = useState ('');
    const [isDisabled, setIsDisabled] = useState(true);


    const navigate = useNavigate();

    const { productId } = useParams();  
    const { _id } = useParams(); 
    
    useEffect(()=>{
        let fieldReqs = (name !== "" && description !== "" && price !== "" && stock !== "")
        if(fieldReqs){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    },[name, description, price, stock,])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/productId:/${productId}`)
          .then((response) => response.json())
          .then((product) => {
            setName(product.name ?? '');
            setDescription(product.description ?? '');
            setPrice(product.price ?? '');
            setStock(product.stock ?? '');
          });
      }, []);
      console.log(productId)
      


      function updateProduct(e) {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/product/updateProduct:/${productId}`, {
          method: 'PATCH',
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify({
            name: name,
            description: description,
            price: price,
            stock: stock
          }),
        })
          .then((response) => response.json())
          .then((data) => {
            if (data) {
              Swal2.fire({
                'title' : 'Update Success',
                icon: 'success',
                text: 'Product has been updated'
              })
              navigate("/dashboard");
            } else {
                Swal2.fire({
                    'title' : 'Update Failed',
                    icon: 'error',
                    text: 'There was an error in updating the product. Please try again.'
                  })
                  
            }
          });
      }
      
    return(
        <Container className="mt-5 pt-3">
            <Row>
                <h2 className="text-center text-light">Product Update</h2>
                <p className="text-center text-light">Please fill the following fields</p>
                <Col>
                    <Form onSubmit={(e) => updateProduct(e)}>
                        <Card>
                          <Card.Body>
                          <Form.Group controlId="formName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                            type="text"
                            value={name}
                            onChange={(e) => setName(e.target.value)}/>
                        </Form.Group>

                        <Form.Group controlId="formDescription">
                            <Form.Label>Product Description</Form.Label>
                            <Form.Control
                            type="text"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}/>
                        </Form.Group>

                        <Form.Group controlId="formPrice">
                            <Form.Label>Product Price</Form.Label>
                            <Form.Control
                            type="text"
                            value={price}
                            onChange={(e) => setPrice(e.target.value)}/>
                        </Form.Group>

                        <Form.Group controlId="formStock">
                            <Form.Label>Stock Number </Form.Label>
                            <Form.Control
                            type="text"
                            value={stock}
                            onChange={(e) => setStock(e.target.value)}/>
                        </Form.Group>
                          </Card.Body>

                          <Card.Footer>
                          <small className="d-grid gap-2">
                            <Button  className="rounded-0" type="submit" disabled={isDisabled}>Update Product</Button>
                            
                          </small>
                         
                          </Card.Footer>
                        </Card>

                        
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}