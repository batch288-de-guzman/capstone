const mongoose = require(`mongoose`);

const userSchema = new mongoose.Schema({
    firstName : {
        type: String,
        required: [true, `first name required`]
    },
    middleName:{
        type : String,
        required: false
    },
    lastName:{
        type : String,
        required : [true, `Last name required`]
    },
    mobileNo : {
        type: Number,
        required : [true, `Contact Number required`]
    },
    email : {
        type : String,
        required : [true, `Email is required`]
    },
    password : {
        type : String,
        required : [true, `Passowrd is requred`]
    },
    isAdmin : {
        type :Boolean,
        default : false
    },
    orderedProduct : [{
        products : [{
            productId : {
                type : String,
                required : [true, `Product ID is required`]
            },
            productName : {
                type : String,
                requried : [true, `Product name is Required`]
            },
            quantity : {
                type : Number,
                required : [true, `Quantity number is required`]
            }
        }],
        totalAmount : {
            type : Number,
            required : [true, `Total amount is required`]
        },
        purchasedOn : {
            type : Date,
            default : new Date()
        }
    }]
});

const User = mongoose.model(`user`, userSchema);
module.exports = User; 
