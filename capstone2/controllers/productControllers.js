const express = require(`express`);
const Product = require(`../models/productModel.js`);

const auth = require(`../auth.js`);
const router = express.Router();

let notAdminMsg = `You are not an admin. You do not have access to this route.`

//___________add product
module.exports.addProduct = (request, response) =>{
    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        let newProduct = new Product({
            name : request.body.name,
            description : request.body.description,
            price : request.body.price,
            stock : request.body.stock,
            isActive : request.body.isActive
        })
        newProduct.save()
        .then(saved => response.send(true))
        .catch(error => response.send(false))
    }else{
        return response.send(false)
    }
}


//_________________get all products
module.exports.allProducts = (request, response) =>{

    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        Product.find({})
        .then(result => response.send(result))
        .catch(error => response.send(false));
    }else{
        return response.send(false)
    }
}


//_____________active products
module.exports.activeProducts = (request, response) =>{  
        Product.find({isActive : true})
        .then(result => response.send(result))
        .catch(error => response.send(false))
}


//_____________find product by id

module.exports.getProduct = (request, response) =>{
    
    const productId = (request.params.productId)
    Product.findById(productId)
    .then(result => response.send(result))
    .catch(error => response.send(false));

}


//_________update product
module.exports.updateProduct = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const productId = request.params.productId;
  
    let updatedProduct = {
      name: request.body.name,
      description: request.body.description,
      price: request.body.price,
      stock: request.body.stock,
    };
  
    if (userData.isAdmin) {
      if (updatedProduct.stock >= 0) {
        Product.findByIdAndUpdate(productId, updatedProduct)
          .then((result) => response.send(true))
          .catch((error) => response.send(false));
      } else {
        response.status(400).send({ message: "Insufficient stock count" });
      }
    } else {
      return response.send(false);
    }
  };
  


//_______archive products

module.exports.archiveProduct = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const productId = request.params.productId;
  
    if (userData.isAdmin) {
      Product.findByIdAndUpdate(productId, { isActive: false })
        .then((result) => response.send(true))
        .catch((error) => response.send(false));
    } else {
      return response.send(false);
    }
  };

  //update stock count
  module.exports.reduceStockCount = async (request, response) => {
    const productId = request.body.id;
  
    try {
      const product = await Product.findOne({ _id: productId });
  
      if (product && product.stock > 0) {
        product.stock -= 1;
        await product.save();
  
        return response.send(true);
      } else {
        return response.send(false);
      }
    } catch (error) {
      console.error(error);
      return response.send(false);
    }
  };
  





