//establish connection to modules

const express = require(`express`);
const app = express();
const mongoose = require(`mongoose`);
const cors = require(`cors`)

const port = 4000;

const userRoutes = require(`./routes/userRoutes.js`)
const productRoutes = require(`./routes/productRoutes.js`)
const orderRoutes = require(`./routes/orderRoutes.js`)

//mongodb connection

mongoose.connect(`mongodb+srv://admin:admin@batch288deguzman.zygjru8.mongodb.net/APITesting?retryWrites=true&w=majority`,{useNewUrlParser: true, useUnifiedTopology:true});

let db = mongoose.connection;
db.on(`error`, console.error.bind(console,`Connection Error. Can't conncent to database`));
db.once(`open`, ()=>console.log(`Connection Successful. Connected to database`));


//middleware________
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//routing___________

app.use(`/user`, userRoutes);
app.use (`/product`, productRoutes);
app.use(`/order`, orderRoutes);

app.listen(port, ()=>console.log(`server running at ${port}`));