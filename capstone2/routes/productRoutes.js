const express = require(`express`);
const productControllers = require(`../controllers/productControllers.js`);

const auth = require(`../auth.js`);
const router = express.Router();

//____________without params

router.post(`/addProduct`, auth.verify, productControllers.addProduct);
router.get(`/allProduct`, auth.verify, productControllers.allProducts);
router.get(`/activeProducts`, productControllers.activeProducts);



//_________with params

router.get(`/productId:/:productId`, productControllers.getProduct);
router.patch(`/updateProduct:/:productId`, auth.verify, productControllers.updateProduct);
router.patch("/:productId/archiveProduct", auth.verify, productControllers.archiveProduct);
router.put(`/update/:id`, auth.verify, productControllers.reduceStockCount)

module.exports = router;


