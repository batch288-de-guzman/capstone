const orderControllers = require(`../controllers/orderControllers.js`);
const express = require(`express`);
const router = express.Router();


router.get(`/`, orderControllers.orderList);

module.exports = router;